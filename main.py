from functions import m_json
from functions import m_pck

# init
path = "/home/pi/Desktop/calorimetry_home-main/datasheets/setup_newton.json"
metadata = m_json.get_metadata_from_setup(path)
m_json.add_temperature_sensor_serials("/home/pi/Desktop/calorimetry_home-main/datasheets", metadata)
# run
runtime_data = m_pck.get_meas_data_calorimetry(metadata)
# log
m_pck.logging_calorimetry(runtime_data, metadata, "/home/pi/Desktop/calorimetry_home-main/data", "/home/pi/Desktop/calorimetry_home-main/datasheets")
m_json.archiv_json("/home/pi/Desktop/calorimetry_home-main/datasheets","/home/pi/Desktop/calorimetry_home-main/datasheets/setup_newton.json","/home/pi/Desktop/calorimetry_home-main/data")
